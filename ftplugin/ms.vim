set keywordprg=sdcv
set spell


fu! Mkdoc ()
	!~/scripts/mkdoc.sh '%'
endfunction

:command Date :put=strftime('%m/%d/%y')
:command G :w |  !~/scripts/mkdoc.sh '%'
:command O   !~/scripts/opendoc.sh '%'&

"template remaps
nnoremap ,ms :-1read /home/lucas/.vim/templates/skeleton.ms<CR>:filetype detect<CR>
nnoremap ,cp :-1read /home/lucas/.vim/templates/groff(cover page).ms<CR>:filetype detect<CR>
inoremap """ \*Q \*U<ESC>3hi
inoremap ___ .FS<ESC>o.FE<ESC>O

" autoindent
if &tw == 0
  setlocal tw=80
endif
