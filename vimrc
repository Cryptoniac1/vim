set relativenumber
set number
set hlsearch
set wildmenu
set wildmode=list:longest,full
set isfname+=32
set autoread
set autoindent
filetype on
filetype plugin on
set omnifunc=syntaxcomplete#Complete
au CursorHold * checktime  
syntax on
colorscheme monokai
let $BASH_ENV = "~/.vim/bash_aliases"
let $VIM='~/.vim/'
hi Normal guibg=NONE ctermbg=NONE
hi Search ctermbg=NONE ctermfg=202
hi SpellBad ctermbg=red ctermfg=black
packloadall

"remaps
nnoremap <silent><C-t> :tabnew<CR>
noremap ,o :execute ":!xdg-open " . shellescape( '<cfile>' )<CR>
nnoremap W :w <CR> 
nnoremap gf <C-W>gf
vnoremap gf <C-W>gf
"Wayland Paste
noremap ,wp :r !wl-paste<CR> 
nmap gy :e<Space>#<CR>
nmap <Leader>r :source ~/.vimrc<CR>
nmap <space> zz
"map <C-c> :!xclip -f -sel clip<CR>
"map <C-p> :-1r !xclip -o -sel clip<CR>
inoremap jj <ESC>
nmap <Leader>td :packadd termdebug<CR> :Termdebug<CR>

"
" Greek letters
inoremap <C-a> α
inoremap <C-b> β
inoremap <C-g> γ
inoremap <C-d> Δ

